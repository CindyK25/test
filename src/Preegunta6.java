import java.util.Scanner;

public class Preegunta6 {
    public static void main(String[] args) {
        String sensor1,estadoSensor1, sensor2, estadoSensor2, sensor3, estadoSensor3, persona;
        int puntaje1, puntaje2, puntaje3, puntajeTotal;

        Scanner input=new Scanner(System.in);

        System.out.println("Ingrese el sensor 1:");
        sensor1=input.nextLine();

        System.out.println("Ingrese el estado del sensor 1: ");
        estadoSensor1=input.nextLine();

        System.out.println("Ingrese el sensor 2:");
        sensor2=input.nextLine();

        System.out.println("Ingrese el estado del sensor 2: ");
        estadoSensor2=input.nextLine();

        System.out.println("Ingrese el sensor 3:");
        sensor3=input.nextLine();

        System.out.println("Ingrese el estado del sensor 3: ");
        estadoSensor3=input.nextLine();

        puntaje1=calcularPuntajeXSensor(sensor1, estadoSensor1);
        System.out.printf("El puntaje de severidad para el sensor 1 es: %d \n",puntaje1);

        puntaje2=calcularPuntajeXSensor(sensor2,estadoSensor2);
        System.out.printf("El puntaje de severidad para el sensor 2 es: %d \n", puntaje2);

        puntaje3=calcularPuntajeXSensor(sensor3,estadoSensor3);
        System.out.printf("El puntaje de severidad para el sensor 3 es: %d \n", puntaje3);

        puntajeTotal=calcularPuntajeTotal(sensor1,estadoSensor1,sensor2,estadoSensor2,sensor3,estadoSensor3);
        System.out.printf("El puntaje total de severidad es: %d \n",puntajeTotal);

        persona=obtenerPersonaContactar(puntajeTotal);
        System.out.printf("La persona a contactar es: %s \n",persona);
    }

    private static int calcularPuntajeXSensor(String sensor, String estadoSensor){
        int puntaje=0;

        switch (estadoSensor){
            case "ok":
                puntaje=0;
                break;
            case "error":
                if(sensor.equalsIgnoreCase("luz"))
                    puntaje=1;
                else if (sensor.equalsIgnoreCase("humo"))
                    puntaje=2;
                else if(sensor.equalsIgnoreCase("puerta"))
                    puntaje=3;
                break;
            case "desconectado":
                if(sensor.equalsIgnoreCase("luz") || sensor.equalsIgnoreCase("humo"))
                    puntaje=3;
                else if (sensor.equalsIgnoreCase("puerta"))
                    puntaje=4;
                break;
        }

        return puntaje;
    }

    private static int calcularPuntajeTotal(String sensor1, String estadoSensor1, String sensor2, String estadoSensor2, String sensor3, String estadoSensor3){
        int puntaje_total;

        int puntaje1=calcularPuntajeXSensor(sensor1, estadoSensor1);
        int puntaje2=calcularPuntajeXSensor(sensor2, estadoSensor2);
        int puntaje3=calcularPuntajeXSensor(sensor3,estadoSensor3);

        puntaje_total=puntaje1+puntaje2+puntaje3;

        return puntaje_total;
    }

    private static String obtenerPersonaContactar(int puntajeTotal){
        String persona="";

        if(puntajeTotal==0)
            persona="Ninguno";
        else if (puntajeTotal>=1 && puntajeTotal<=3)
            persona="Jefe de Operaciones";
        else if (puntajeTotal>=4 && puntajeTotal<=7)
            persona="Supervisor de Operaciones";
        else if (puntajeTotal>=8)
            persona="Gerente de Operaciones";

        return persona;
    }

}
