import java.util.Scanner;

public class Ejercicio2 {
    public static void main(String[] args) {
        int tipo_intervencion;
        double sin_intervencion, con_intervencion;

        Scanner input=new Scanner(System.in);
        //Rinoplastia=1
        //Liposuccion=2
        //Mamoplastia=3

        System.out.println("\n Tipos de intervencion: \n1=Rinoplastia \n2=Liposuccion \n3=Mamoplastia");
        System.out.println("\nIngresar tipo de intervención: "); //2
        tipo_intervencion=input.nextInt();

        sin_intervencion=Calcular_precio_sin_internamiento(tipo_intervencion);
        con_intervencion=Calcular_precio_con_internamiento(tipo_intervencion);

        System.out.println("Precio en (S/) sin internamiento es: "+sin_intervencion);
        System.out.printf("Precio en (S/) con internamiento es: "+con_intervencion);

    }

    private static double Calcular_precio_sin_internamiento(int tipo_intervencion){
        double precio;

        if(tipo_intervencion==1){
            precio=1800*2.9;
        } else if (tipo_intervencion==2) {
            precio=4500*2.9;
        } else {
            precio=3500*2.9;
        }

        return precio;
    }

    private static double Calcular_precio_con_internamiento(int tipo_intervencion){
        double precio_total;

        if(tipo_intervencion==1){
            precio_total=(1800*2.9)+(1000);
        } else if (tipo_intervencion==2) {
            precio_total=(4500*2.9)+(4000);
        } else {
            precio_total=(3500*2.9)+(2000);
        }

        return precio_total;
    }
}
